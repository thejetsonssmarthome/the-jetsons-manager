<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?= $this->fetch('title') ?>
        </title>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    

        <style type="text/css">
            body a:hover {
                color: #194f8c;
            }
        </style>

    </head>
    <?= $this->fetch('content') ?>
</html>
