<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Dispositivos') ?> - <?= h($device->name) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $device->name ? $device->name : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Grupo') ?></p>
                        <p><?= $device->has('group') ? $this->Html->link($device->group->name, ['controller' => 'Groups',
                            'action' => 'view', $device->group->id]) : 'Não Informado' ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $device->status ? __('Ligado') : __('Desligado'); ?></p>

                        <p class="title"><?= __('Último sinal') ?></p>
                        <p><?= $device->last_heartbeat?></p>


                        <p class="title"><?= __('Últim atividade') ?></p>
                        <p><?= $device->last_activity?></p>


                        <p class="title"><?= __('Criado em') ?></p>
                        <p><?= $device->created?></p>


                        <p class="title"><?= __('Habilitado no sistema') ?></p>
                        <p><?= $device->active ? __('sim') : __('Não'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $device->id], ['class' => "btn
                        btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $device->id], ['class' => "btn
                        btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?',
                        $device->id)]) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


