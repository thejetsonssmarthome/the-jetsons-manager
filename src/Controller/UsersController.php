<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => []
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }


    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($id == null) { //get the user on session
            $id = $this->Auth->user('id');
        }

        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->created = date('Y-m-d H:i:s');
            $user->user_id = $this->Auth->user('id');
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Saved successfully'));

                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('An error occurred, please try later.'));
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Removed successfully'));
        } else {
            $this->Flash->error(__('An error occurred, please try later.'));
        }

        return $this->redirect($this->referer());
    }

    public function login()
    {

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->registraLogin($this->Auth->user('id'));
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid Username and/or Password'));
        }
        $user = $this->Users->newEntity();
        $this->set('user', $user);
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }


    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'login', 'logout', 'esquecisenha', 'redefinirsenha']);
    }

    public function registraLogin($userid)
    {
        $user = $this->Users->get($userid);
        $user->lastlogin = date('Y-m-d H:i:s');
        $this->Users->save($user);
    }

    /**
     * Esquecisenha method
     *
     * @return \Cake\Network\Response|void Redirects on successful, renders view otherwise.
     */
    public function esquecisenha()
    {

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->find()->where(['email' => $this->request->data('email')])->first();

            if ($user) {
                $this->emailRecuperacaoSenha($user);
                $this->Flash->success(__('Você receberá instruções por email para a recuperação de senha. Verifique também na sua caixa de Spam.'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('Não encontramos usuário com esse email informado.'));
            }
        }
        $this->set(compact('user'));
        $this->set('user', $user);
    }

    /*
     * opção de redefinição de senha, caso o usuário tenha esquecido
     */

    public function redefinirsenha($token)
    {
        $user = $this->Users->find()->where(['md5(email)' => $token])->first();

        if (empty($user)) {
            $this->Flash->error(__('Esse Link para redefinição de senha não é mais válido.'));
            return $this->redirect(['action' => 'login']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!empty($this->request->data('novasenha')) && $this->request->data('novasenha') === $this->request->data('repetirnovasenha')) {
                $user->password = $this->request->data('novasenha');

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Sua senha foi redefinida. Você já pode acessar o sistema novamente.'));
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error(__('Não foi possível redefinir a senha. Verifique se preencheu os campos corretamente.'));
                }
            } else {
                $this->Flash->error(__('As senhas devem ser iguais para poder redefiní-la'));
            }
        }

        $this->set(compact('user'));
        $this->set('user', $user);
    }

    /*
        * email disparado para usuario com link para recuperação de senha
        */

    public function emailRecuperacaoSenha($user)
    {
        try {
            $email = new Email('default');
            $email->viewVars(['nome' => $user->name, 'usuario' => $user->username, 'cripto' => md5($user->email), 'domain' => $this->request->domain()]);
            $email->subject('[The Jetsons] Recuperação de Senha')
                ->template('default', 'recuperacaosenha')
                ->emailFormat('html')
                ->to($user->email)
                ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email de confirmação para o email informado. Confira se está correto, por favor.'));
        }
    }
}
