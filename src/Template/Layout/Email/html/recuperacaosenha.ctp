<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Lato:400);
        /* Take care of image borders and formatting */

        img {
            max-width: 600px;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a {
            text-decoration: none;
            border: 0;
            outline: none;
        }

        a img {
            border: none;
        }

        /* General styling */

        td,
        h1,
        h2,
        h3 {
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 400;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100%;
            height: 100%;
            color: #37302d;
            background: #ffffff;
        }

        /*            table {
                        background:
                    }*/

        h1,
        h2,
        h3 {
            padding: 0;
            margin: 0;
            color: #ffffff;
            font-weight: 400;
        }

        h3 {
            color: #21c5ba;
            font-size: 24px;
        }
    </style>
    <style type="text/css" media="screen">
        @media screen {
            /* Thanks Outlook 2013! http://goo.gl/XLxpyl*/
            td,
            h1,
            h2,
            h3 {
                font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
            }
        }
    </style>
    <style type="text/css" media="only screen and (max-width: 480px)">
        /* Mobile styles */

        @media only screen and (max-width: 480px) {
            table[class="w320"] {
                width: 320px !important;
            }

            table[class="w300"] {
                width: 300px !important;
            }

            table[class="w290"] {
                width: 290px !important;
            }

            td[class="w320"] {
                width: 320px !important;
            }

            td[class="mobile-center"] {
                text-align: center !important;
            }

            td[class="mobile-padding"] {
                padding-left: 20px !important;
                padding-right: 20px !important;
                padding-bottom: 20px !important;
            }
        }
    </style>
</head>
<body bgcolor="#ffffff" class="body"
      style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
<table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <tbody>
    <tr>
        <td align="center" valign="top" bgcolor="#ffffff" width="100%">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-bottom: 3px solid #3bcdc3;" width="100%">
                    </td>
                </tr>

                <tr>
                    <td valign="top">
                        <center contenteditable="true">
                            <table cellspacing="0" cellpadding="30" width="500" class="w290">
                                <tbody>
                                <tr>
                                    <td valign="top" style="border-bottom:1px solid #a1a1a1;">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="color:#2E64FE;text-align: center;">
                                                    <h3>Olá <?= $nome ?>,
                                                        <br>
                                                    </h3>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                <tbody>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td class="mobile-padding" style="text-align:left;">
                                                    <br>Você nos solicitou a recuperação de senha no sistema
                                                    <br>Caso você tenha esquecido o seu usuário, verifique-o abaixo
                                                    <br>Seu usuário para login é: <?= $usuario ?>
                                                    <br><br>Caso tenha esquecido a sua senha, acesse abaixo para
                                                    redefinição de senha
                                                    <br><br>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="mobile-padding">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="width:150px; background-color: #2E64FE">
                                                    <a href="http://<?= $domain ?>/users/redefinirsenha/<?= $cripto ?>"
                                                       target="_blank">
                                                        <div style="text-align: center;">
                                                            <font color="#ffffff">
                                                        <span style="font-size: 13px;">
                                                            <b>&nbsp;Redefinir Senha</b>
                                                        </span>
                                                            </font>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="25" width="100%">
                                            <tbody>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#c2c2c2;">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>


</body>
</html>
