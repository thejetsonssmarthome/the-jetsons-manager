<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"> </i> <?= __('Grupo') ?> - <?= h($group->name) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $group->name ? $group->name : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $group->status ? __('Ativo') : __('Desabilitado'); ?></p>


                        <p class="title"><?= __('Criado em') ?></p>
                        <p><?= $group->created?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $group->id], ['class' => "btn
                        btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $group->id], ['class' => "btn
                        btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?',
                        $group->id)]) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php if (!empty($group->devices)) { ?>
<div class="x_panel">
    <div class="x_title">
        <h2 class="green"><i class="fa fa-file"></i> Dispositivos desse Grupo</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action">
                <thead>
                <tr class="headings">
                    <th scope="col" class="column-title">Dispositivo</th>
                    <th scope="col" class="column-title">Status</th>
                    <th scope="col" class="column-title">Ult Atividade</th>
                    <th scope="col" class="column-title">Ult Sinal</th>
                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                </tr>
                </thead>
                <tbody>

                <?php $cor = 'even';
                    foreach ($group->devices as $device) {
                ?>
                <tr class="<?= $cor ?> pointer">
                    <td><?= $this->Html->link($device->name, ['controller' => 'Devices', 'action' => 'view',
                        $device->id]) ?>
                    </td>
                    <td><?= $device->status ? 'Ligado' : 'Desligado'?></td>
                    <td><?= h($device->last_activity) ?></td>
                    <td><?= h($device->last_heartbeat) ?></td>
                    <td class=" last">
                        <div class="btn-group">
                            <?= $this->Html->link(__('Visualizar Dispositivo'), ['controller' => 'Devices', 'action' =>
                            'view', $device->id], ['class' => "btn btn-primary btn-xs"]) ?>
                        </div>
                    </td>
                </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>
