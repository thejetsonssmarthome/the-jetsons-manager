<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Dispositivos') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('name', 'Dispositivo') ?>
                            </th>
                            <th scope="col" class="column-title">Grupo</th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('status', 'Status') ?></th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('last_activity', 'Ult
                                Atividade') ?>
                            </th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('last_heartbeat', 'Ult
                                Sinal') ?>
                            </th>
                            <th scope="col" class="column-title"><?= $this->Paginator->sort('active', 'Habilitado?')
                                ?>
                            </th>

                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">( <span
                                    class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $cor = 'even';
                            foreach ($devices as $device){ ?>
                        <tr class="<?= $cor ?> pointer">
                            <td><?= h($device->name) ?></td>
                            <td><?= $device->has('group') ? $this->Html->link($device->group->name, ['controller' =>
                                'Groups',
                                'action' => 'view', $device->group->id]) : '' ?>
                            <td><?= $device->status ? 'Ligado' : 'Desligado'?></td>
                            <td><?= h($device->last_activity) ?></td>
                            <td><?= h($device->last_heartbeat) ?></td>
                            <td><?= $device->active ? 'Sim' : 'Não'?></td>
                            </td>

                            <td class=" last">
                                <div class="btn-group">
                                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $device->id], ['class' =>
                                    "btn btn-primary btn-xs"]) ?>
                                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $device->id],
                                    ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja
                                    remover esse registro?', $device->id)]) ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próxima') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
