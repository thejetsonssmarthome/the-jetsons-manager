<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Conta de Usuário</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Name') ?></p>
                        <p><?= $user->name  ?></p>

                        <p class="title"><?= __('Username') ?></p>
                        <p><?= $user->username ?></p>


                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $user->email  ?></p>

                        </div>
                </div>



                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Lastlogin') ?></p>
                        <p><?= $user->lastlogin ? $user->lastlogin : 'sem informação'; ?></p>


                        <p class="title"><?= __('Created') ?></p>
                        <p><?= $user->created ? $user->created : 'sem informação'; ?></p>


                        <p class="title"><?= __('Updated') ?></p>
                        <p><?= $user->updated ? $user->updated : 'sem informação'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $user->status ? __('Ativo') : __('Desativado'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?php if ($loggeduser == $user->id) { ?>
                            <?= $this->Html->link("Editar", ['action' => 'edit', $user->id], ['class' => "btn btn-primary"]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


