<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Device Entity
 *
 * @property string $id
 * @property string|null $name
 * @property int|null $group_id
 * @property \Cake\I18n\FrozenTime|null $last_activity
 * @property string|null $last_heartbeat
 * @property bool|null $status
 * @property bool|null $active
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Group $group
 */
class Device extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'group_id' => true,
        'last_activity' => true,
        'last_heartbeat' => true,
        'status' => true,
        'active' => true,
        'created' => true,
        'group' => true
    ];
}
