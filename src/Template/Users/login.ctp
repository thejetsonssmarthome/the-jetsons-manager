<!DOCTYPE html>
<html lang="pt">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>The Jetsons</title>
    <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>

    <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
    <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
    <?= $this->Html->css('/vendors/animate.css/animate.min.css') ?>
    <?= $this->Html->css('custom.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

</head>


<body class="login">
<div>
    <a class="hiddenanchor" id="signin"></a>
    <a class="hiddenanchor" id="signup"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <?= $this->Form->create() ?>
                <h1>The Jetsons</h1>
                <div>
                    <?php echo $this->Html->image("rosie-robot_p.jpg", ["alt" => "The
                    Jetsons",'width'=>'200px','height'=>'180px']); ?>
                </div>
                <p><?= $this->Flash->render() ?></p>
                <p><?= $this->Flash->render('auth') ?></p>
                <div>
                    <?= $this->Form->input('username', ['label' => false, "class" => "form-control", "placeholder" =>
                    "Usuário"]) ?>
                    <?= $this->Form->input('password', ['label' => false, "class" => "form-control", "placeholder" =>
                    "Senha"]) ?>
                </div>
                <div>
                    <?= $this->Form->button(__('Entrar'), ["class" => "btn btn-default submit"]); ?>
                    <a class="reset_pass" href="esquecisenha/">Esqueceu sua senha?</a>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">Novo no sistema?
                        <a href="#signup" class="to_register"> Crie uma conta </a>
                    </p>

                </div>
                <?= $this->Form->end() ?>
            </section>
        </div>

        <div id="register" class="animate form registration_form">
            <section class="login_content">
                <?= $this->Form->create($user, ['url' => ['controller' => 'Users', 'action' => 'add']]); ?>
                <h1>The Jetsons</h1><br>
                <h2>Cadastro</h2>
                <div>
                    <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" required=""/>
                </div>
                <div>
                    <input type="text" id="username" name="username" class="form-control" placeholder="Usuário"
                           required=""/>
                </div>
                <div>
                    <input type="email" id="email" name="email" class="form-control" placeholder="Email" required=""/>
                </div>
                <div>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Senha"
                           required=""/>
                </div>
                <div>
                    <button class="btn btn-default submit" type="submit">Cadastrar</button>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">Já é cadastrado? Faça
                        <a href="#signin" class="to_register"> Login </a>
                    </p>

                </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
</html>
