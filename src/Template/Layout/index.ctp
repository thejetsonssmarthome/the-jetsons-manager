<!DOCTYPE html>
<html lang="de">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>The Jetsons</title>

    <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
    <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
    <?= $this->Html->css('custom.min.css') ?>

    <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false)) ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <style type="text/css">
        body a:hover {
            color: #194f8c;
        }
    </style>

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                            <span class="site_title">
                                The Jetsons
                            </span>
                </div>

                <div class="clearfix"></div>

                <!--menu profile quick info-->
                <div class="profile">
                    <div class="profile_pic">
                        <?php echo $this->Html->image("profiles/default.png", ["height" => 70, "class" => 'img-circle
                        profile_img', 'url' => ['controller' => 'Users', 'action' => 'view']]); ?>
                    </div>
                    <div class="profile_info">
                        <span>Bem Vindo,</span>
                        <h2><?= $loggednome ?></h2>
                    </div>
                </div>
                <br/>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Sistema Admin</h3>
                        <ul class="nav side-menu">
                            <!--para cada menu-->
                            <li><a><i class="fa fa-dashboard"></i> Dashboard <span
                                class="fa fa-chevron-down"></span></a>
                                <!--para cada submenu-->
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Em Tempo Real', ['controller' => 'Devices', 'action' =>
                                        'dashboard']); ?>
                                    </li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-home"></i> Dispositivos <span class="fa fa-chevron-down"></span></a>
                                <!--para cada submenu-->
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Ver todos', ['controller' => 'Devices', 'action' =>
                                        'index']); ?>
                                    </li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-cube"></i> Grupos <span class="fa fa-chevron-down"></span></a>
                                <!--para cada submenu-->
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Ver todos', ['controller' => 'Groups', 'action' =>
                                        'index']); ?>
                                    </li>
                                    <li><?= $this->Html->link('Criar novo', ['controller' => 'Groups', 'action' => 'add']);
                                        ?>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <?= $loggedfirstname ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li> <?= $this->Html->link('Minha conta', ['controller' => 'users', 'action' => 'view'])
                                    ?>
                                </li>
                                <li> <?= $this->Html->link('Log out', ['controller' => 'users', 'action' => 'logout'])
                                    ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' =>
            false]); ?>
            <?= $this->Flash->render('auth') ?>
            <?= $this->fetch('content') ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                The Jetsons - Automação Residencial
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>
<?= $this->Html->script('custom.min.js') ?>

<?= $this->fetch('script') ?>
</body>
</html>
