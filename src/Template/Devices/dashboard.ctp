<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_content">
            <?= $this->Flash->render() ?>
            <div class="row" id="content-box">

                <?php foreach($groups as $group){?>

                <div class="col-md-3 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><?= $group->name ?>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <?php if($group->devices){
                            foreach($group->devices as $device){?>
                            <p><?= $device->name ?></p>
                            <a class="switch" id="<?= str_replace(':','',$device->id) ?>"
                               href="javascript:void(0)"
                               onclick="toggle('<?= $device->id ?>')">
                                <?php if($device->status){ ?>
                                <?= $this->Html->image("btn_on.png", ['class' => 'switch-on']) ?>
                                <?php }else {?>
                                <?= $this->Html->image("btn_off.png", ['class' => 'switch-off']) ?>
                                <?php }?>
                            </a>
                            <?php }
                            }?>
                        </div>
                    </div>
                </div>

                <?php }?>
            </div>
        </div>
    </div>
    <div style="display: none">
        <span id="img-btn-on"><?= $this->Html->image("btn_on.png", ['class' => 'switch-on']) ?></span>
        <span id="img-btn-off"> <?= $this->Html->image("btn_off.png", ['class' => 'switch-off']) ?></span>
    </div>
    <?= $this->Form->input('urlroot', ["type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
</div>
<script>
    var img_btn_on = $('#img-btn-on').html();
    var img_btn_off = $('#img-btn-off').html();

    function switchStatus(device_id, new_status) {

        var data = {
            'endpointId': device_id,
            'newStatus': new_status
        };

        $.ajax({
            url: $('#urlroot').val() + 'devices/switchStatus?' + $.param(data),
            type: 'GET',
            success: function (response) {
                console.log(response);
            }
        });
    }

    function toggle(device_id) {
        var btnSwitch = $('#' + device_id.split(":").join(""));
        if (btnSwitch.find('.switch-on').length > 0) {
            btnSwitch.html(img_btn_off);
            switchStatus(device_id, 'OFF');
        } else {
            btnSwitch.html(img_btn_on);
            switchStatus(device_id, 'ON');
        }
    }

</script>
