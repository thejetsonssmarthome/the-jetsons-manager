<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;


/**
 * Devices Controller
 *
 * @property \App\Model\Table\DevicesTable $Devices
 *
 * @method \App\Model\Entity\Device[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DevicesController extends AppController
{


    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['all', 'switchStatus']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Groups']
        ];
        $devices = $this->paginate($this->Devices);

        $this->set(compact('devices'));
    }

    /**
     * all method
     *
     * @return \Cake\Http\Response|null
     */
    public function all()
    {
        $devices = $this->Devices->find('all')->contain(['Groups']);
        $this->set(compact('devices'));
        $this->set('_serialize', ['devices']);
        $this->setJsonResponse();
    }

    public function dashboard()
    {
        $groups = $this->Devices->Groups->find('all')->contain(['Devices']);
        $this->set(compact('groups'));
        $this->set('_serialize', ['groups']);
    }


    /**
     * switch method
     *
     * @return \Cake\Http\Response|null
     */
    public function switchStatus()
    {
        $msg = $this->request->getQuery('newStatus') == 'ON' ? 1 : 0;
        $endpointId = $this->request->getQuery('endpointId');
        $topic = "/DEVICE/$endpointId/SWITCH";

        if ($this->publish_message($msg, $topic))
            $this->sendSuccessResponse();
        else
            $this->sendFailureResponse();

    }


    /**
     * View method
     *
     * @param string|null $id Device id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $device = $this->Devices->get($id, [
            'contain' => ['Groups']
        ]);

        $this->set('device', $device);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $device = $this->Devices->newEntity();
        if ($this->request->is('post')) {
            $device = $this->Devices->patchEntity($device, $this->request->getData());
            if ($this->Devices->save($device)) {
                $this->Flash->success(__('The device has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The device could not be saved. Please, try again.'));
        }
        $groups = $this->Devices->Groups->find('list', ['limit' => 200]);
        $this->set(compact('device', 'groups'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Device id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $device = $this->Devices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $device = $this->Devices->patchEntity($device, $this->request->getData());
            if ($this->Devices->save($device)) {
                $this->Flash->success(__('The device has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The device could not be saved. Please, try again.'));
        }
        $groups = $this->Devices->Groups->find('list', ['limit' => 200]);
        $this->set(compact('device', 'groups'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Device id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $device = $this->Devices->get($id);
        if ($this->Devices->delete($device)) {
            $this->Flash->success(__('The device has been deleted.'));
        } else {
            $this->Flash->error(__('The device could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
