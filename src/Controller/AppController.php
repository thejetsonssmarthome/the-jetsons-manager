<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use \Mosquitto\Client;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $paginate = [
        'limit' => 10
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Devices',
                'action' => 'dashboard'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'finder' => 'auth'
                ]
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'authError' => 'unauthorized access',
            'storage' => 'Session'
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        if ($this->request->session()->read('Auth.User.id')) {
            if ($this->request->is('ajax') || $this->viewBuilder()->layout() == 'ajax') {
                $this->viewBuilder()->layout('ajax');
            } else {
                $this->viewBuilder()->layout('index');
            }

            $name = $this->request->session()->read('Auth.User.name');
            $this->set("loggednome", $name);

            $loggedfirstname = explode(' ', $name);
            $this->set("loggedfirstname", $loggedfirstname[0]);

            $this->set("backlink", $this->referer());
            $this->set("loggeduser", $this->request->session()->read('Auth.User.id'));
            $this->set("loggedIn", true);
        } else {
            $this->set("loggedIn", false);
        }
    }

    public function beforeFilter(Event $event)
    {
        if (!$this->Auth->user('id') && $this->request->getData('token')) {
            /*  $this->loadModel('Users');
              $user = $this->Users->find()->where(['token' => $this->request->data('token')])->first();
              if (!$user) $this->Auth->config('authError', false);
              else $this->directLogin($user);*/
        }
    }

    public function directLogin($user)
    {
        $this->Auth->setUser($user);
    }

    public function setJsonResponse()
    {
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');
    }

    public function sendSuccessResponse()
    {
        $response['response'] = 'success';
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
        $this->setJsonResponse();
    }

    public function sendFailureResponse()
    {
        $response['response'] = 'fail';
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
        $this->setJsonResponse();
    }

    function publish_message($msg, $topic)
    {
        $mid = null;
        $client = new \Mosquitto\Client('PHP-APP');
        $client->onConnect(function() use ($client, &$mid, &$msg, &$topic) {
            $mid = $client->publish($topic, $msg, 1);
        });
        $client->onPublish(function() use ($client) {
                $client->disconnect();
        });
        $client->connect(Configure::read('MQTT.host'), Configure::read('MQTT.port'), 5);
        $client->loopForever();
        unset($client);
        return $mid;
    }

}
