<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>The Jetsons</title>
        <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    

    </head>

    <body class="login">
        <div>
            <a class="hiddenanchor" id="signin"></a>
            <a class="hiddenanchor" id="signup"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <?= $this->Form->create($user); ?>
                        <h1>The Jetsons</h1>
                        <h2>Redefinir senha</h2>
                        <div>
                            <?php echo $this->Html->image("rosie-robot_p.jpg", ["alt" => "The
                            Jetsons",'width'=>'200px','height'=>'180px']); ?>
                        </div>
                        <p><?= $this->Flash->render() ?></p>
                        <div class="form-group">
                            <?= $this->Form->input('novasenha', ['type'=>'password',"class" => "form-control", "placeholder" => "Digite a nova senha", 'label' => false]) ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->input('repetirnovasenha', ['type'=>'password',"class" => "form-control", "placeholder" => "Repita a nova senha", 'label' => false]) ?>
                        </div>
                        <div>
                            <button class="btn btn-primary submit" type="submit">Redefinir Senha</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">Fazer
                                <?= $this->Html->link(__('Login'), ['action' => 'login']) ?>
                            </p>

                        </div>
                        <?= $this->Form->end() ?>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
