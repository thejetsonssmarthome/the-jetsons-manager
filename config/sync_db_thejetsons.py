from datetime import datetime
import json
import mysql.connector
import paho.mqtt.client as mqtt


def get_time():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def get_group_by_name(name):
    query = "SELECT * FROM groups WHERE name = %s"
    value = (name,)
    cursor.execute(query, value)
    print(" - procurando por um grupo existente")
    return cursor.fetchone()


def add_group(name):
    query = 'INSERT INTO groups(name, status, created) VALUES(%s, %s, %s)'
    values = (name, 1, get_time())
    cursor.execute(query, values)
    mydb.commit()
    print(" - novo grupo inserido")
    return cursor.lastrowid


def add_device(device):
    try:
        group = get_group_by_name(device['group'])

        if (group is None):
            group_id = add_group(device['group'])
        else:
            print(group)
            print(group[0])
            group_id = group[0]

        query = 'INSERT INTO devices(id, name, group_id, created, last_activity, last_heartbeat, status, active) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)'
        values = (device['id'], device['name'], group_id, get_time(), get_time(), get_time(), 1, 0)
        cursor.execute(query, values)
        mydb.commit()
        print(" - novo dispositivo inserido")

    except mysql.connector.Error as err:
          print(err)


def update_status_device(device):
    try:
        query = 'UPDATE devices set status = %s, last_activity = %s WHERE id = %s'
        values = (device['status'], get_time(), device['id'])
        cursor.execute(query, values)
        mydb.commit()
        print(" - atividade do dispositivo ", device['name'], " foi registrado")

    except mysql.connector.Error as err:
        print(err)


def update_heartbeat_device(device):
    try:
        query = 'UPDATE devices set last_heartbeat = %s WHERE id = %s'
        values = (get_time(), device['id'])
        cursor.execute(query, values)
        mydb.commit()
        print(" - heartbeat recebido do dispositivo ", device['name'])

    except mysql.connector.Error as err:
        print(err)


def on_message(client, userdata, msg):
    print('mensagem: ', json.loads(msg.payload))
    print('topico: ', msg.topic)
    global heartbeat_topic
    global new_device_topic
    global status_device_topic

    data = json.loads(msg.payload)

    if msg.topic == heartbeat_topic:
        update_heartbeat_device(data)
    elif msg.topic == new_device_topic:
        add_device(data)
    elif msg.topic == status_device_topic:
        update_status_device(data)


def on_connect(client, userdata, flags, rc):
    print("Conectando ao MQTT Broker...")
    on_intial_subscription(client)


def on_intial_subscription(client):
    global heartbeat_topic
    global new_device_topic
    global status_device_topic

    print("Conectado...")

    heartbeat_topic = "/DEVICE/HEARTBEAT"
    new_device_topic = "/DEVICE/NEW"
    status_device_topic = "/DEVICE/STATUS"

    client.subscribe(heartbeat_topic)
    client.subscribe(new_device_topic)
    client.subscribe(status_device_topic)


def db_connect():
    global cursor
    global mydb

    print("Conectando com o banco de dados...")
    mydb = mysql.connector.connect(host="127.0.0.1", port=3306, user="thejetsons", password="123",
                                   database="the_jetsons")

    cursor = mydb.cursor()


def mqtt_server_connect():
    mqttIP = "192.168.15.8"
    mqttPort = 1883

    client = mqtt.Client("BD_SYNC_THEJETSONS")
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(mqttIP, mqttPort)
    client.loop_forever()


db_connect()
mqtt_server_connect()
